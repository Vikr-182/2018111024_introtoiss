.. Assignment-4 documentation master file, created by
   sphinx-quickstart on Fri Apr 19 17:15:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Context-Free Grammer's webpage
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   Context Free Grammar is a formal grammar consisting of some rules. These rules can tell us whether the string or a sentence is valid or not. A valid sentence can be represented using these rules in the form of tree called phrase structure tree. In other words, these rules are used for parsing the sentences.Context Free Grammar is not good for free word order language such as Hindi. This concept is largely used in a language like English for parsing. 



Reference to pages
==================

* :ref:`Quizzes`
* :ref:`Experiment`

