var DOCUMENTATION_OPTIONS = {
    URL_ROOT: '',
    VERSION: '2019-04-19',
    LANGUAGE: 'None',
    COLLAPSE_INDEX: false,
    FILE_SUFFIX: '.html',
    HAS_SOURCE: true,
    SOURCELINK_SUFFIX: '.txt'
};