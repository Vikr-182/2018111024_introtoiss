ican=0
if [[ "$1" = "" ]]
then 
	echo "Please enter a URL."
else
	#Check for URL validity
	if [[ $(wget "$1" -O-) ]] 2>/dev/null
	then 
		echo "Ok"
		ican=1
	else
		echo "Please enter a valid URL."
	fi
fi
if [[ $ican -eq 1 ]]
then 
	echo search 
	if [[ "$2" = "" ]]
	then
		echo "Please enter a string to search for"
	else
	#	count=`wget -qO- https://unix.stackexchange.com/questions/42636/how-to-get-text-of-a-page-using-wget-without-html | sed -e 's/<[^>]*>//g;s/^ //g' | wc -l`
	#	let var=count-30
		wget -qO- "$1" | sed -e 's/<[^>]*>//g;s/^ //g' > res.txt
		echo "$2"
		cnt=`cat res.txt | grep -c "$2"`
		echo "The string has occured $cnt times."
	fi
fi
