if [[ ! -e ./songslists.csv ]]
then
	touch songslists.csv
	echo "File created"
	echo -e "Sr.No\tSong_name\tArtist\tGenre\tDecription\t" >> songslists.csv
fi

declare songname
ican=0

if [ "$1" = "" ]
then
	echo "Please add a song name."
fi

if [ "$2" = "" ]
then
        echo "Please add the song's artist ."
fi

if [ "$3" = "" ]
then
        echo "Please add a song genre."
fi

if [ "$4" = "" ]
then
        echo "Please add some description to the song."	
else
	count=`cat ./songslists.csv | wc -l`
	for (( i=2;i<=count;i++ ))
	do
		string=`cat ./songslists.csv | cut  -f 2 | head -"$i" | tail -1 |  sed -e 's/^[[:space:]]*//'  `
	#	echo $string Ra
		if test "$string" = "$1"
		then 
			ican=1						
		fi
	done
	#echo "ican =  $ican"
if [[ ican -eq 0 ]]
then
	echo -e "$count\t$1\t\t$2\t$3\t$4\t" >> songslists.csv
else
	echo "Song is aleady in your list."	
fi

fi
