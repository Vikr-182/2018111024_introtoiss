#!/bin/bash
count=0
if [[ "$1" == "" ]]
then 
	echo "Please add the number of iterations."
	count=1
fi

if [[ ! -e /usr/bin/notify-send ]]
then
	echo "Installing notify-send"
	sudo apt install notify-osd		
fi

if [[ ! -e /usr/bin/at ]]
then
        echo "Installing at command"
        sudo apt install at
fi

for(( i=1;i <= $1 ;i++  ))
do
	if (( "$i" % 4 !=0 ))
	then
		notify-send "Task.No '$i'." "Work" | at now +5min
		notify-send "Task.No $i" "Break-Time" | at now +25min
	else
		notify-send "Task.No $i" "Large Break" | at now +15min
	fi
done
