veryhappy=`printf '\U1F929\n'`
happy=`printf '\U1F917\n'`
ok=`printf '\U1F934\n'`
if [[ "$1" = "" ]]
then
	echo "Please add a number"
else
	for(( i=1;i<="$1";i++ ))
	do
		var=$(($i%4))
                if [[ $var -eq 0 ]]
                then
                        ( sleep $((1*$i)) ; echo "Long Break $veryhappy" ) &
                fi  
		if [[  $var -ne 0 ]]
		then
			( sleep  $((2*$i)) ; echo "Work $ok"  ) &
			#echo "HiRa $i"
			( sleep  $((4*$i)) ; echo "Break-time $happy" ) &
			#echo "Howareyou $i"
		fi
	done
fi
