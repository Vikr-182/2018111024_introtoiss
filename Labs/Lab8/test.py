from matrix import Matrix

m = Matrix.makeRandom(3,3)
m1 = Matrix.makeRandom(3,3)


print(m)
print(m1)
print(m+m1)
print(m*m1)
#print(m**2)     # Multiplies matrix by a scalar
print(m<m1)
print(m<=m1)
print(m>m1)
print(m>=m1)
print(m==m1)
print(m!=m1)
